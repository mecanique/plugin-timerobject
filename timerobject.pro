######################################################################
# Project file implemented for qmake (version 3 with Qt 5)
######################################################################

include(../plugin/plugin.pri)
TARGET = timerobject
DEPENDPATH += .
INCLUDEPATH += .
RESOURCES += resources.qrc
TRANSLATIONS = translations/timerobject.fr.ts \
               translations/timerobject.de.ts

OTHER_FILES += metadata.json

HEADERS +=	    MecTimerObjectEditor.h \
                MecTimerFunctionEditor.h \
                MecTimerSignalEditor.h \
                MecTimerObjectCompiler.h \
                MecTimerObjectPlugin.h
              	


SOURCES +=	    MecTimerObjectEditor.cpp \
                MecTimerFunctionEditor.cpp \
                MecTimerSignalEditor.cpp \
                MecTimerObjectCompiler.cpp \
                MecTimerObjectPlugin.cpp
                


