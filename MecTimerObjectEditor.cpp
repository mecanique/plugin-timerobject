/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecTimerObjectEditor.h"

MecTimerObjectEditor::MecTimerObjectEditor(MecAbstractObject* const Object, MecAbstractEditor* MainEditor, QWidget * Parent, Qt::WindowFlags F) : MecObjectEditor(Object, MainEditor, Parent, F)
{
tabWidgetMain->removeTab(tabWidgetMain->indexOf(widgetVariables));
}

MecTimerObjectEditor::~MecTimerObjectEditor()
{
}
	
bool MecTimerObjectEditor::canAddFunction() const
{
return false;
}

bool MecTimerObjectEditor::canAddSignal() const
{
return false;
}

bool MecTimerObjectEditor::canAddVariable() const
{
return false;
}

bool MecTimerObjectEditor::canRemoveChild(MecAbstractElement* const Element) const
{
return false;
}
	
MecAbstractElementEditor* MecTimerObjectEditor::newSubEditor(MecAbstractElement *Element)
{
if (Element->elementRole() == MecAbstractElement::Function)
	{
	MecElementEditor *tempEditor = new MecTimerFunctionEditor(static_cast<MecAbstractFunction*>(Element), mainEditor());
	tempEditor->childListElementsChanged(Element);
	tabWidgetFunctions->addTab(tempEditor, QIcon(":/share/icons/types/" + Element->elementType() + ".png"), Element->elementName());
	return tempEditor;
	}
else if (Element->elementRole() == MecAbstractElement::Signal)
	{
	MecElementEditor *tempEditor = new MecTimerSignalEditor(static_cast<MecAbstractSignal*>(Element), mainEditor());
	tempEditor->childListElementsChanged(Element);
	tabWidgetSignals->addTab(tempEditor, QIcon(":/share/icons/types/" + Element->elementType() + ".png"), Element->elementName());
	return tempEditor;
	}
else return 0;
}
	
void MecTimerObjectEditor::addFunction() {}
void MecTimerObjectEditor::addSignal() {}
void MecTimerObjectEditor::addVariable() {}


