/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECTIMEROBJECTEDITOR_H__
#define __MECTIMEROBJECTEDITOR_H__

#include <MecObjectEditor.h>
#include "MecTimerFunctionEditor.h"
#include "MecTimerSignalEditor.h"

/**
\brief	Classe d'édition d'objet de type « Timer ».
*/
class MecTimerObjectEditor : public MecObjectEditor
{
	Q_OBJECT

public:
	/**
	\brief	Constructeur.
	\param	Object	Objet édité, doit absolument exister lors de la construction (c.à.d. instancié et différent de 0) sinon un comportement inattendu pourrait se produire.
	*/
	MecTimerObjectEditor(MecAbstractObject* const Object, MecAbstractEditor* MainEditor, QWidget * Parent=0, Qt::WindowFlags F=0);
	/**
	\brief	Destructeur.
	*/
	~MecTimerObjectEditor();

	///Indique si une MecFunction peut être ajouté, retourne donc \e false.
	bool canAddFunction() const;
	///Indique si un MecSignal peut être ajouté, retourne donc \e false.
	bool canAddSignal() const;
	///Indique si une MecVariable peut être ajouté, retourne donc \e false.
	bool canAddVariable() const;
	///Indique si un élément enfant peut être supprimé, retourne donc \e false.
	bool canRemoveChild(MecAbstractElement* const Element) const;

public slots:
	/**
	\brief	Demande un sous-éditeur pour \e Element.
	\note	\e Element doit être un élément enfant de element().
	\return	Le sous-éditeur demandé, ou 0 si éditer cet élément n'est pas possible.
	*/
	MecAbstractElementEditor* newSubEditor(MecAbstractElement *Element);

	///Ne fait rien.
	void addFunction();
	void addSignal();
	void addVariable();

signals:


private:


};

#endif /* __MECTIMEROBJECTEDITOR_H__ */

