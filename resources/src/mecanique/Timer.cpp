/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "Timer.h"

Timer::Timer(QString Name, Project* const Project) : Object(Name, "Timer", Project)
{
m_timer = new QTimer(this);
	m_timer->setSingleShot(true);
	connect(m_timer, SIGNAL(timeout()), this, SIGNAL(timeout()));
	connect(m_timer, SIGNAL(timeout()), this, SLOT(timerTimeout()));
changeStatus(Object::Operational);
changeInfos(tr("Timer ready."));
}

Timer::~Timer()
{
}
	
void Timer::more()
{
}
	
QVariant Timer::settings()
{
return QVariant();
}

#pragma GCC diagnostic ignored "-Wunused-parameter"
void Timer::setSettings(QVariant Settings)
{
}
#pragma GCC diagnostic pop

void Timer::start(int Milliseconds)
{
m_timer->start(Milliseconds);
if (m_timer->isSingleShot())
	{
	project()->log()->write(name(), "Timer started in single-shot mode with " + QString::number(m_timer->interval()) + "ms delay.");
	changeInfos(tr("Timer started with %1 milliseconds. Wait...").arg(QString::number(m_timer->interval())));
	}
else 
	{
	project()->log()->write(name(), "Timer started in loop mode with " + QString::number(m_timer->interval()) + "ms delay.");
	changeInfos(tr("Timer started in loop with %1 milliseconds delay.").arg(QString::number(m_timer->interval())));
	}
}

void Timer::stop()
{
m_timer->stop();
project()->log()->write(name(), "Timer stopped.");
changeInfos(tr("Timer ready."));
}
	
int Timer::interval()
{
return m_timer->interval();
}
	
void Timer::singleShot(bool Single)
{
m_timer->setSingleShot(Single);
}

bool Timer::isSingleShot()
{
return m_timer->isSingleShot();
}

void Timer::timerTimeout()
{
project()->log()->write(name(), "Timeout.");
if (m_timer->isSingleShot()) stop();//Permet de réinitialiser le texte.
}


