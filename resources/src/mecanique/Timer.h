/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __TIMER_H__
#define __TIMER_H__

#include "Object.h"

/**
\brief	Classe de gestion de temporisateur.
*/
class Timer : public Object
{
	Q_OBJECT
	
	public:
	/**
	\brief	Constructeur.
	
	\param	Name	Nom de l'objet.
	\param	Projet	Projet d'appartenance.
	*/
	Timer(QString Name, Project* const Project);
	///Destructeur.
	virtual ~Timer();
	
	///Ne fait rien.
	virtual void more();
	
	///Retourne un QVariant vide.
	virtual QVariant settings();
	///Ne fait rien.
	virtual void setSettings(QVariant Settings);
	
	public slots:
	///Démarre le temporisateur.
	void start(int Milliseconds);
	///Arrête le temporisateur.
	void stop();
	
	///Retourne l'intervalle en vigueur.
	int interval();
	
	///Fixe l'unicité du déclenchement du temporisateur.
	void singleShot(bool Single);
	///Indique si le timer est en coup unique (true), ou non (false).
	bool isSingleShot();
	
	signals:
	///Est émis lorsque le temps est écoulé.
	void timeout();
	
	private:
	///Temporisateur.
	QTimer *m_timer;
	
	private slots:
	///Réinitialise le texte d'information.
	void timerTimeout();
	
};

#endif /* __TIMER_H__ */



