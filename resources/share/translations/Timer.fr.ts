<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>Timer</name>
    <message>
        <location filename="../../src/mecanique/Timer.cpp" line="57"/>
        <source>Timer started with %1 milliseconds. Wait...</source>
        <translation>Temporisateur enclenché avec %1 millisecondes. Attente…</translation>
    </message>
    <message>
        <location filename="../../src/mecanique/Timer.cpp" line="62"/>
        <source>Timer started in loop with %1 milliseconds delay.</source>
        <translation>Temporisateur enclenché en boucle avec %1 millisecondes d&apos;intervalle.</translation>
    </message>
    <message>
        <location filename="../../src/mecanique/Timer.cpp" line="29"/>
        <location filename="../../src/mecanique/Timer.cpp" line="70"/>
        <source>Timer ready.</source>
        <translation>Temporisateur prêt.</translation>
    </message>
</context>
</TS>
