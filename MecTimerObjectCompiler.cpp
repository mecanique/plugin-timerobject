/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecTimerObjectCompiler.h"

MecTimerObjectCompiler::MecTimerObjectCompiler(MecAbstractObject* const Object, MecAbstractCompiler* const MainCompiler) : MecObjectCompiler(Object, MainCompiler)
{

}

MecTimerObjectCompiler::~MecTimerObjectCompiler()
{
}

QList<QResource*> MecTimerObjectCompiler::resources()
{
QList<QResource*> tempList;
tempList.append(new QResource(":/share/icons/types/Timer.png"));
tempList.append(new QResource(":/src/mecanique/Timer.h"));
tempList.append(new QResource(":/src/mecanique/Timer.cpp"));

tempList.append(new QResource(":/share/translations/Timer.fr.qm"));
return tempList;
}

QString MecTimerObjectCompiler::projectInstructions()
{
return QString("HEADERS += ./mecanique/Timer.h\nSOURCES += ./mecanique/Timer.cpp\n");
}
	
QString MecTimerObjectCompiler::header()
{
QString tempString("/*\n\
© Quentin VIGNAUD, 2013\n\
\n\
Licensed under the EUPL, Version 1.1 only.\n\
You may not use this work except in compliance with the\n\
Licence.\n\
You may obtain a copy of the Licence at:\n\
\n\
http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available\n\
\n\
Unless required by applicable law or agreed to in\n\
writing, software distributed under the Licence is\n\
distributed on an “AS IS” basis,\n\
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either\n\
express or implied.\n\
See the Licence for the specific language governing\n\
permissions and limitations under the Licence.\n\
*/\n\
\n\
#ifndef __" + object()->elementName().toUpper() + "_H__\n\
#define __" + object()->elementName().toUpper() + "_H__\n\
\n\
#include \"mecanique/Timer.h\"\n\
\n\
class " + object()->elementName() + " : public Timer\n\
{\n\
	Q_OBJECT\n\
	\n\
	public:\n\
	" + object()->elementName() + "(Project* const Project);\n\
	~" + object()->elementName() + "();\n\
	\n\
	\n\
};\n\
\n\
#endif /* __" + object()->elementName().toUpper() + "_H__ */\n\n");
return tempString;
}

QString MecTimerObjectCompiler::source()
{
QString tempString("/*\n\
© Quentin VIGNAUD, 2013\n\
\n\
Licensed under the EUPL, Version 1.1 only.\n\
You may not use this work except in compliance with the\n\
Licence.\n\
You may obtain a copy of the Licence at:\n\
\n\
http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available\n\
\n\
Unless required by applicable law or agreed to in\n\
writing, software distributed under the Licence is\n\
distributed on an “AS IS” basis,\n\
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either\n\
express or implied.\n\
See the Licence for the specific language governing\n\
permissions and limitations under the Licence.\n\
*/\n\
\n\
#include \"" + object()->elementName() + ".h\"\n\
\n\
" + object()->elementName() + "::" + object()->elementName() + "(Project* const Project) : Timer(\"" + object()->elementName() + "\", Project)\n\
{\n\
}\n\
\n\
" + object()->elementName() + "::~" + object()->elementName() + "()\n\
{\n\
}\n\
\n");
return tempString;
}


